import { Container, Text } from "@chakra-ui/react"
import { useAllArticles } from "pages/api/hooks/article/article.queries"
import Header from "@/components/base/header"
import Footer from "@/components/base/footer"

export default function Articles() {
    const { data: article } = useAllArticles()

    return (
        <>
            <Header />
            <main>
                <Container>
                    <Text>Content</Text>
                </Container>
            </main>
            <Footer />
        </>
    )
}