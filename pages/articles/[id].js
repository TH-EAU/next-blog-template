import { Container, Text } from "@chakra-ui/react"
import { useArticle } from "pages/api/hooks/article/article.queries"
import Header from "@/components/base/header"
import Footer from "@/components/base/footer"
import { useRouter } from "next/router"

export default function Article() {
    const router = useRouter()
    const { id } = router.query
    const { data: article } = useArticle ({id}, { enabled: !!id })

    return (
        <>
            <Header />
            <main>
                <Container>
                    <Text>Content : Article - {id}</Text>
                </Container>
            </main>
            <Footer />
        </>
    )
}