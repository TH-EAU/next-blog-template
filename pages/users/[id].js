import { Container, Text } from "@chakra-ui/react"
import { useUser } from "pages/api/hooks/user/user.queries"
import Header from "@/components/base/header"
import Footer from "@/components/base/footer"
import { useRouter } from "next/router"

export default function User() {
    const router = useRouter()
    const { id } = router.query
    const { data: user } = useUser ({id}, { enabled: !!id })

    return (
        <>
            <Header />
            <main>
                <Container>
                    <Text>Content : User - {id}</Text>
                </Container>
            </main>
            <Footer />
        </>
    )
}