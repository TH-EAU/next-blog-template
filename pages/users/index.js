import { Container, Text } from "@chakra-ui/react"
import { useAllUsers } from "pages/api/hooks/user/user.queries"
import Header from "@/components/base/header"
import Footer from "@/components/base/footer"

export default function Users() {
    const { data: user } = useAllUsers()

    return (
        <>
            <Header />
            <main>
                <Container>
                    <Text>Content</Text>
                </Container>
            </main>
            <Footer />
        </>
    )
}