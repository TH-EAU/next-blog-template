import { ChakraProvider } from '@chakra-ui/react'
import { QueryClient, QueryClientProvider, useQueryErrorResetBoundary } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import RenderGate from 'src/utils/RenderGate'

function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient()


  return (
    <QueryClientProvider client={queryClient}>
      <ReactQueryDevtools initialIsOpen={false} />
      <ChakraProvider>
        <RenderGate>
          <Component {...pageProps} />
        </RenderGate>
      </ChakraProvider>
    </QueryClientProvider>
  )
}

export default MyApp
