const keys= {
    user: "user",
    users: "users",
    /* INSERT_KEY */
}

export const keyFactory = {
    user: ({id}) => [keys.user,id],
    users: [keys.users],
    /* INSERT_KEY_SPECS */
}

