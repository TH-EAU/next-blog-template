import { useQuery } from "react-query"
import { getAll, get } from "pages/api/services/UserService"
import { keyFactory } from "pages/api/hooks/user/keyFactory"

export const useAllUsers = (options) => {
    return useQuery(keyFactory.users, getAll, { ...options })
}

export const useUser = ({ id }, options) => {
    return useQuery(keyFactory.user({ id }), () => get({ id }), options)
}