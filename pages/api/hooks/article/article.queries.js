import { useQuery } from "react-query"
import { getAll, get } from "pages/api/services/ArticleService"
import { keyFactory } from "pages/api/hooks/article/keyFactory"

export const useAllArticles = (options) => {
    return useQuery(keyFactory.articles, getAll, { ...options })
}

export const useArticle = ({ id }, options) => {
    return useQuery(keyFactory.article({ id }), () => get({ id }), options)
}