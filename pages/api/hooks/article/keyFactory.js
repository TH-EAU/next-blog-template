const keys= {
    article: "article",
    articles: "articles",
    /* INSERT_KEY */
}

export const keyFactory = {
    article: ({id}) => [keys.article,id],
    articles: [keys.articles],
    /* INSERT_KEY_SPECS */
}

