import generateRoute from "src/utils/generateRoute"


export const get = async ({ id }) => {
    const route = `user/${id}`

    return await generateRoute.get({ route })
}

export const getAll = async () => {
    const route = "users"

    return await generateRoute.get({ route })
}


