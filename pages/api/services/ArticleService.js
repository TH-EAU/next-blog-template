import generateRoute from "src/utils/generateRoute"


export const get = async ({ id }) => {
    const route = `article/${id}`

    return await generateRoute.get({ route })
}

export const getAll = async () => {
    const route = "articles"

    return await generateRoute.get({ route })
}


