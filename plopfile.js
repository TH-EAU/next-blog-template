module.exports = (plop) => {
    plop.setGenerator(
        "collectionCompleteView",
        require('./plop-guides/generators/collectionView.generator')
    );
    plop.setGenerator(
        "collectionService",
        require("./plop-guides/generators/service.generator")
    );
    plop.setGenerator(
        "pages",
        require("./plop-guides/generators/page.generator")
    );
    plop.setGenerator(
        "component",
        require("./plop-guides/generators/component.generator")
    );
};
