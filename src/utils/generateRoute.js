import axios from "axios"

const BASE_PATH = "http://localhost:1337/api/"

const get = async ({ route, populate = false, params = {} }) => {
    return await axios.get(`${BASE_PATH}${route}?${populate ? 'populate=*' : ''}`)
}

const generateRoute = {
    get: get
}

export default generateRoute