import React from "react";
import { QueryErrorResetBoundary } from "react-query";
import { ErrorBoundary } from "react-error-boundary";

import { Button } from "@chakra-ui/button";

export default function RenderGate({ loader, children }) {
    return (
        <QueryErrorResetBoundary>
            {({ reset }) => (
                <ErrorBoundary onReset={reset} fallbackRender={({ error, resetErrorBoundary }) => (
                    <Button onClick={() => resetErrorBoundary()}> Retry</ Button>

                )}>
                    {children}
                </ErrorBoundary>
            )}
        </QueryErrorResetBoundary>
    )
}
